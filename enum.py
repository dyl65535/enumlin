#!/usr/bin/env python3
"""
This module is designed to enumerate a host and post the results into ELK 
"""
import sys
import re
import json
import jc
import base64
import socket
from datetime import datetime
from elasticsearch import Elasticsearch
from fabric import Connection
from scapy.all import *

__author__ = "D"
__version__ = "0.1.0"
__license__ = "MIT"

#global vars are bad, TODO: fix it to take kwargs from cmdline or env file.
ES_SOCKET = ('172.16.3.5', 9200)

class HostEnum():
    def __init__(self):
        self.EnumType = "Host"
        self.OS = {
            "Linux":{
                "Version":"",
            },
            "ComputerSystem":{
                "Name": "",
                "Model":"",
                "DNSHostName":"",
            },
            "System Time": ""
        }
        self.NET = {
            "Netstat": ({"LocalAddress":"","LocalPort":0,"RemoteAddress":"","RemotePort":0,"OwningProcess":0}),
            "IpAddress": "",
            "Raw":{
                "Netstat": "",
                "UDPStat": "",
                "Ipconfig":"",
                "Arp":"",
                "Route":"",
                "FirewallRules":"",
                "FirewallSetting":"",
                "DNSHistory":"",
            },
            "Count": {
                "Netstat": 0,
                "UDPStat": 0,
                "Ipconfig":0,
                "Arp":0,
                "Route":0,
                "FirewallRules":0,
                "FirewallSetting":0,
                "DNSHistory":0,
            }
        }
        self.USER = {
            "Accounts":({}),
            "Groups":({}),
            "Raw":{
                "LocalUsers":"",
                "LocalAdmins":"",
            },
            "Count": {
                "LocalUsers":0,
                "LocalAdmins":0,
            }
        }

        self.SOFTWARE = {
            "Package": (),
            "Count": 0
        }

        self.HW = {
            "PCI": (),
            "NetworkCards": (),
            "Disks": (),
            "USB": (),
            "CPU": "",
            "Memory":"",
            "Raw":{
                "Drivers":"",
                "Firmware":""
            }
        }

        self.PS = {
            "Processes": ({"Name":"placeholder","ProcessId":1,"ParentProcessId":1,"CommandLine":"placeholder"}),
            "Count": 1,
            "Raw": "placeholder"
        }



    def enum_lin(self):
        c = Connection(self.remoteIP, user=self.credentials["username"], connect_kwargs={"password":self.credentials["password"]})
        self.timestamp = datetime.utcnow().isoformat()

        #OS
        self.OS["Linux"]["Version"] = c.run("uname -a", hide='out').stdout
        self.OS["ComputerSystem"]["Name"] = c.run("hostname", hide='out').stdout
        self.OS["ComputerSystem"]["Model"] = "placeholder"
        self.OS["ComputerSystem"]["DNSHostName"] = "placeholder"
        self.OS["System Time"] = {"DateTime":c.run("date", hide='out').stdout,"Value":c.run("date", hide='out').stdout}
        #USER
        self.USER["Accounts"] = ({"Name":"holder","Disabled":False})
        self.USER["Groups"] = ({"Group":"Test","Member":"placeholder"})
        self.USER["Raw"]["LocalUsers"] = c.run("cat /etc/passwd", hide='out').stdout
        self.USER["Raw"]["LocalAdmins"] = "placeholder"
        self.USER["Count"]["LocalUsers"] = 1
        self.USER["Count"]["LocalUsers"] = 1
        #SOFTWARE
        self.SOFTWARE["Package"] = ("a","b","c")
        self.SOFTWARE["Count"] = 1
        #HW
        self.HW["PCI"] = ({"DeviceID":"abc","Name":"placeholder"})
        self.HW["NetworkCards"] = ({"InterfaceAlias":"name","MacAddress":"abcd","Status":"placeholder"})
        self.HW["Disks"] = ({"DeviceID":"abc","DriveType":0,"FreeSpace":100,"Size":100})
        self.HW["USB"] = ({"DeviceID":"abc","Name":"placeholder"})
        self.HW["CPU"] = c.run("lscpu", hide='out').stdout
        self.HW["Memory"] =({"Size":1,"Slot":"placeholder"}) #c.run("lsmem", hide='out').stdout
        self.HW["Raw"]["Drivers"] = "placeholder"
        self.HW["Raw"]["Firmware"] = "placeholder"
        #NET
        self.NET["Netstat"] = ({"LocalAddress":"palcehold","LocalPort":1,"RemoteAddress":"palcehold","RemotePort":1,"OwningProcess":1})
        self.NET["IpAddress"] = c.run("ip addr", hide='out').stdout
        self.NET["Raw"]["Netstat"] = c.run("ss", hide='out').stdout
        self.NET["Raw"]["UDPStat"] = c.run("ss -tu", hide='out').stdout
        self.NET["Raw"]["Ipconfig"] = c.run("ip a", hide='out').stdout
        self.NET["Raw"]["Arp"] = c.run("ip neighbour", hide='out').stdout
        self.NET["Raw"]["Route"]= c.run("ip route", hide='out').stdout
        self.NET["Raw"]["FirewallRules"] = "placeholder" #c.run("iptables -L").stdout
        self.NET["Raw"]["FirewallSetting"] = "placeholder"
        self.NET["Count"]["DNSHistory"] = 1
        self.NET["Count"]["Netstat"] = 1
        self.NET["Count"]["UDPStat"] = 1
        self.NET["Count"]["Ipconfig"] = 1
        self.NET["Count"]["Arp"] = 1
        self.NET["Count"]["Route"] = 1
        self.NET["Count"]["FirewallRules"] = 1
        self.NET["Count"]["FirewallSetting"] = 1
        self.NET["Count"]["DNSHistory"] = 1
        #PA
        self.PS["Raw"] = c.run("ps -ef", hide='out').stdout
        self.PS["Processes"] = [{"Name":x['uid'],"ProcessId":x['pid'],"ParentProcessId":x['ppid'],"CommandLine":x['cmd']} for x in jc.parse('ps',self.PS["Raw"])]
        self.PS["Count"] = len(self.PS["Processes"])

        # self.raw_services = c.run("systemctl -a", hide='out').stdout
        # self.services = [x for x in jc.parse('systemctl', self.raw_services)]
        # self.raw_jobs = c.run("crontab -l", hide='out').stdout
        # self.jobs = [x for x in jc.parse('crontab', self.raw_jobs)]
        # self.tmp = c.run("ls -la /tmp", hide='out').stdout
        # self.raw_lsof = c.run("lsof", hide='out').stdout
        # self.lsof = [x for x in jc.parse('lsof', self.raw_lsof)]
        # self.raw_hosts = c.run("cat /etc/hosts", hide='out').stdout
        # self.hosts = [x for x in jc.parse('hosts', self.raw_hosts)]
        # self.netstat = [x for x in jc.parse('ss', self.raw_netstat )]

    def enumerate(self, ip, **kwargs):
        #check for valid ip
        if not re.match(r"(?:\d{1,3}\.){3}\d{1,3}", ip):
            print("Invalid IP.")
            return
        self.remoteIP = ip

        #should we fingerprint
        if kwargs.get("fingerprint"):
            self.type = fingerprint_os(ip)
        elif kwargs.get("os"):
            self.type = kwargs.get("os")

        if self.type == "Linux":
            print("commence lin enum")
            self.enum_lin()
        else:
            print("unknown remote OS.. quitting")

    def toJSON(self):
        retval = json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, separators=(',',':'))
        #weird regex to remove creds from the json.. idk how to do this better.
        sanitized = re.sub(r'"credentials":\{.*?\},','', retval)
        return sanitized

    def index_doc(self):
        try:
            client = connect_es(ES_SOCKET, self.credentials['elkauth'])
            testvar = self.toJSON()
            response = client.index(index="testenum", document=testvar)
            print("client.index response:", response)
        except Exception as error:
            print("client.index() error: ", type(error))
            print("client.index() error: ", error)



def connect_es(skt,elkauth):
    """ A simple unauthenticated connection to ES. Takes socket tuple, returns an elasticsearch instance thats connected. """
    addr,port = skt
    es = None
    es = Elasticsearch([{'scheme':'https','host': addr, 'port': port}],verify_certs=False, http_auth=(elkauth.split(":")))
    if es.ping():
        print('Connection Established to ES')
    else:
        print('Connection to ES FAILED')
        return None
    return es

def enum_linpeas(c):
    """ takes a fabric connections, runs linpeas, returns base64 encoded result (bytes)"""
    c.run('rm -rf /tmp/enum')
    c.run('mkdir /tmp/enum')
    c.put('linpeas.sh', '/tmp/enum/linpeas.sh')
    c.run('chmod +x /tmp/enum/linpeas.sh')
    c.run('/tmp/enum/linpeas.sh -N > /tmp/enum/out.txt')
    res =  c.run('cat /tmp/enum/out.txt').stdout
    c.run('rm -rf /tmp/enum')
    return base64.b64encode(bytes(res,'utf-8'))

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.settimeout(0)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

def get_mac(ip):
    # Create arp packet object. pdst - destination host ip address
    arp_request = ARP(pdst=ip)
    # Create ether packet object. dst - broadcast mac address. 
    broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
    # Combine two packets in two one
    arp_request_broadcast = broadcast/arp_request
    # Get list with answered hosts
    answered_list = srp(arp_request_broadcast, timeout=1,verbose=False)[0]
    # Return host mac address
    print(answered_list)
    return answered_list[0][1].hwsrc

def fingerprint_os(ip):
    """ takes an ip, and returns a string. Guesses what the os is based upon handshake TTL """

    fing_sport = 8888
    dport_list = [22,445]
    fing_src = get_ip()
    fing_dst = ip
    resp_ttl = 0
    success = False

    for fing_dport in dport_list:
        print(f"Attempting port {fing_dport}")
        #my_ETH = Ether(src=Ether().src,dst=get_mac(fing_dst))
        my_IP = IP(src=fing_src,dst=fing_dst)
        my_SYN = TCP(sport=fing_sport,dport=fing_dport,flags="SE",seq=1000)
        my_SYNACK = sr1(pinmy_IP/my_SYN, timeout=2, verbose=False)
        if not my_SYNACK:
            print("Timeout. Attempting next port")
            continue
        else:
            my_ACK = TCP(sport=fing_sport,dport=fing_dport,flags="RA",seq=my_SYNACK.ack, ack=my_SYNACK.seq + 1)
            send(my_IP/my_ACK, verbose=False)
            resp_ttl = my_SYNACK.ttl
            success = True
            break
    if success:
        if resp_ttl <= 64:
            return "Linux"
        elif resp_ttl > 64 and resp_ttl <= 128:
            return "Windows"
        else:
            return "Unknown"
    else: 
        return "Unknown"

def main(**kwargs):
    """ Main entry point of the file """

    if not kwargs.get('username') and not kwargs.get('password'):
        print("Please specify credentials:  enum.py username=<blah> password=<blah>")
        return   

    if not kwargs.get('elkauth'):
        print("You did not specify elk index creds. elkauth=<user>:<pass>")
        return

    e1 = HostEnum()
    e1.credentials ={"username":kwargs.get('username'), "password":kwargs.get('password'), "elkauth":kwargs.get('elkauth')}
    e1.enumerate("172.16.3.10", os="Linux")
    e1.index_doc()
    

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main(**dict(arg.split('=') for arg in sys.argv[1:]))